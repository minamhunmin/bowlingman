import Vue from 'vue'
import moment from 'moment'
import axios from 'axios'

import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.prototype.$moment = moment
Vue.prototype.$axios = axios

// debugger
if (!firebase.apps.length) {
    firebase.initializeApp({
        apiKey: process.env.FIREBASE_APIKEY,
        projectId: process.env.FIREBASE_PID,
        authDomain: process.env.FIREBASE_URL
    })
}

Vue.prototype.$db = firebase.firestore();